/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_Planets_private__ = __webpack_require__(4);
/**
 * Created by maibo on 25.11.2017.
 */


const earth = Object(__WEBPACK_IMPORTED_MODULE_0__modules_Planets_private__["a" /* default */])("earth");
earth.rotatePlanet();

































































































































































































/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by maibo on 30.11.2017.
 */
const Planets = (name) => {
    const populationMultiplyRate = () => {
        return Math
            .floor(Math.random() * (10 - 1) + 1)
    };
    const randomPopulation = () => {
        return Math
            .floor(Math.random() * (1000000000 - 1000000) + 10000000)
    };
    const Cataclysm = function () {
        let x = Math
                .floor(Math.random() * (10 - 1) + 1)*10000;
        let different = randomPopulation()-x;
        console.log(`от катаклизма погибло ${different} человек`)
    };

     const state = {
            name: name,
            population: randomPopulation(),
            timeCycle: 24,

            growPopulation: () => {
            let addPopulation = populationMultiplyRate() * (1 / 1000 * randomPopulation());
            let resultPopulation = Math.floor(addPopulation + randomPopulation());
            console.log(`за один цикл прибавилось ${resultPopulation} населения планеты`)
            },

            rotatePlanet: () => {
             let x = Math
                .floor(Math.random() * (10 - 1) + 1);
           x % 2 !== 0 ? Cataclysm() : state.growPopulation()
             }
    };
    return state.rotatePlanet()
};
/* harmony default export */ __webpack_exports__["a"] = (Planets);

/***/ })
/******/ ]);