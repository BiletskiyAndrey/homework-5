/**
 * Created by maibo on 25.11.2017.
 */

import {Fly,Eat,Run} from "./metod_Birds"

const Birds = (name, eat) => {
    let state = {
        name,
        eat
    };
    return Object.assign(
        {},
        Fly(state),
        Eat(state),
        Run(state)
    )
};

export default Birds