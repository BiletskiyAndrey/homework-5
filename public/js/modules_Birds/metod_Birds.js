/**
 * Created by maibo on 25.11.2017.
 */


const Fly = (state) =>({
    fly: () => console.log(`${state.name} can fly`)
});

const Eat = (state) => ({
    eat: () => console.log(`${state.name} eat ${state.eat} `)
});

const Run = (state) => ({
    run:() => console.log(`${state.name} can run  `)
});

export {Fly, Eat, Run}