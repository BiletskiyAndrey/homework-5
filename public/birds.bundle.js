/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modules_Birds_Constructor_Birds__ = __webpack_require__(1);
/**
 * Created by maibo on 25.11.2017.
 */


const gull = Object(__WEBPACK_IMPORTED_MODULE_0__modules_Birds_Constructor_Birds__["a" /* default */])("Чайка", "Corn");
gull.fly();
gull.eat();
gull.run();

const owl = Object(__WEBPACK_IMPORTED_MODULE_0__modules_Birds_Constructor_Birds__["a" /* default */])("Olw","Children");
owl.fly();
owl.eat();






/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__metod_Birds__ = __webpack_require__(2);
/**
 * Created by maibo on 25.11.2017.
 */



const Birds = (name, eat) => {
    let state = {
        name,
        eat
    };
    return Object.assign(
        {},
        Object(__WEBPACK_IMPORTED_MODULE_0__metod_Birds__["b" /* Fly */])(state),
        Object(__WEBPACK_IMPORTED_MODULE_0__metod_Birds__["a" /* Eat */])(state),
        Object(__WEBPACK_IMPORTED_MODULE_0__metod_Birds__["c" /* Run */])(state)
    )
};

/* harmony default export */ __webpack_exports__["a"] = (Birds);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Fly; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Eat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Run; });
/**
 * Created by maibo on 25.11.2017.
 */


const Fly = (state) =>({
    fly: () => console.log(`${state.name} can fly`)
});

const Eat = (state) => ({
    eat: () => console.log(`${state.name} eat ${state.eat} `)
});

const Run = (state) => ({
    run:() => console.log(`${state.name} can run  `)
});



/***/ })
/******/ ]);