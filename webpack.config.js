/**
 * Created by maibo on 25.11.2017.
 */
const path = require('path');
module.exports = {
    entry: {
        birds:'./public/js/main_birds.js',
        planet:'./public/js/main_planets.js'
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].bundle.js'
    },
    devServer: {
        contentBase: './public'
    }
};