/**
 * Created by maibo on 30.11.2017.
 */
const Planets = (name) => {
    const populationMultiplyRate = () => {
        return Math
            .floor(Math.random() * (10 - 1) + 1)
    };
    const randomPopulation = () => {
        return Math
            .floor(Math.random() * (1000000000 - 1000000) + 10000000)
    };
    const Cataclysm = function () {
        let x = Math
                .floor(Math.random() * (10 - 1) + 1)*10000;
        let different = randomPopulation()-x;
        console.log(`от катаклизма погибло ${different} человек`)
    };

     const state = {
            name: name,
            population: randomPopulation(),
            timeCycle: 24,

            growPopulation: () => {
            let addPopulation = populationMultiplyRate() * (1 / 1000 * randomPopulation());
            let resultPopulation = Math.floor(addPopulation + randomPopulation());
            console.log(`за один цикл прибавилось ${resultPopulation} населения планеты`)
            },

            rotatePlanet: () => {
             let x = Math
                .floor(Math.random() * (10 - 1) + 1);
           x % 2 !== 0 ? Cataclysm() : state.growPopulation()
             }
    };
    return state
};
export default Planets